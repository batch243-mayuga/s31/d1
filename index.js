// Use the "require" directive to load the HTTP module of node JS

// A "module" is a software component or part of a program that contains one or more routines

// The "http modules" let Node.js transfer data using the HTTP (Hypertext Transfer Protocol)

// HTTP is a protocol that allows the fetching of resources such as HTML documents

// Client (browser) and servers (nodeJS/ExpressJS application) communicate in exchanging individual messages

// REQUEST - messages sent by the client (usually in a Web browser)
// RESPONSES - messages sent by the server as an answer to the client

let http = require("http")

// CREATE A SERVER
/* -The http module has a createSever()method that accepts a function as an argument and allows server creation 
	-The arguments passed in the functions are request and response objects (data type) that contain method that allows us to receive requests from the client and send the responses back
	- Using the module's createServer() method, we create an HTTP server that listens to the requests on a specified port and gives back to the client
*/


// Define the Port number that the server will be listening to

http.createServer(function(request,response){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Hello World')
}).listen(4000)

console.log('Server running at localhost: 4000');

// A port is a virtual point where network connections start and end

// Send a response back to the client
// Use the writeHead() method
// Set a status code for the response - a 200 - OK

// Inputting the command tells our device to run node js
// node index.js

// Install nodemon via terminal
// npm install -g nodemon

// To check if nodemon is already installed
// nodemon -v

/*
	IMPORTANT NOTE
	-Installing the package will allow the server to automatically restart when files have been changed or updated
	"-g" refers to a global installation where the current version of the package/dependency will be installed locally on our device allowing us to use nodemon on any project
*/