/*
CREATE A ROUTE
	"/greeting"

*/
const http = require('http')

let url = require('url')

// Creates a variable 'port' to store the port number
const port = 4000

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request,response) => {

		// Accessing the 'greeting' route returns a message of 'Hello World'
		if(request.url == '/greeting'){

			response.writeHead(200,{'Content-Type' : 'text/plain'})
			response.end('Hello Again')

		// Accessing the 'homepage' route returns a message of "This is Homepage"
		} else if(request.url == '/homepage'){
			response.writeHead(200, {'Content-Type' : 'text/plain'})
			response.end('Welcome Home')

		// All other routes will return a message of 'Page not available'
		} else {
			response.writeHead(404, {'Content-Type' : 'text/plain'})
			response.end('Page not available')
		}
		
})

// Uses the "server" and "port" variables above
server.listen(port)

// When the server is running, console will print this message:
console.log(`Server now accessible at localhost:${port}`);